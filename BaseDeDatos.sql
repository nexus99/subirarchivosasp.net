CREATE DATABASE practica1;
GO
USE practica1;
GO
CREATE TABLE usuarios
(
	id_usuario		INT IDENTITY(1,1) PRIMARY KEY,
	nombre			VARCHAR(500) NOT NULL,
	apellidos		VARCHAR(500) NOT NULL,
	correo			VARCHAR(300) UNIQUE NOT NULL,
	contrasena		VARCHAR(MAX) NOT NULL,
	Nacimiento		DATE	NOT NULL		
);
GO

CREATE TABLE documentos
(
	id_documento	INT IDENTITY(1,1) PRIMARY KEY,
	documento		VARCHAR(250) NOT NULL,
	extension		VARCHAR(5) NOT NULL,
	registro		DATE DEFAULT(GETDATE()),
	ruta			VARCHAR(MAX)
);
GO