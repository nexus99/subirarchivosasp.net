﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="registro.aspx.cs" Inherits="Practica1.registro" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>registro</title>
    <link href="Utelerias/bootstrap.min.css" rel="stylesheet" />
    <style>
        body {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            display:flex;
            justify-content:center;
            align-items:center;
        }

        .carta-fix {
            width:25%;
            padding:24px;
        }
    </style>
</head>
<body>
    <form class="card carta-fix" id="form1" runat="server">
    <div>

        <div class="form-group">
            <asp:Label AssociatedControlID="txtNombre" runat="server" Text="Nombre"></asp:Label>
            <asp:TextBox ID="txtNombre" CssClass="form-control" required="required" runat="server"></asp:TextBox>
        </div>

        <div class="form-group">
            <asp:Label AssociatedControlID="txtApellidos" runat="server" Text="Apellidos"></asp:Label>
            <asp:TextBox ID="txtApellidos" CssClass="form-control" required="required" runat="server"></asp:TextBox>
        </div>

        <div class="form-group">
            <asp:Label AssociatedControlID="txtCorreo" runat="server" Text="Correo"></asp:Label>
            <asp:TextBox ID="txtCorreo" CssClass="form-control" required="required" runat="server"></asp:TextBox>
        </div>

        <div class="form-group">
            <asp:Label AssociatedControlID="txtNacimiento" runat="server" Text="Fecha de Nacimiento"></asp:Label>
            <asp:TextBox ID="txtNacimiento" CssClass="form-control" TextMode="Date" required="required" runat="server"></asp:TextBox>
        </div>
            
        <div class="form-group">
            <asp:Label ID="lblContrasena" AssociatedControlID="txtContrasena" runat="server" Text="Constraseña"></asp:Label>
            <asp:TextBox ID="txtContrasena" required="required" CssClass="form-control" TextMode="Password" runat="server"></asp:TextBox>
        </div>

        <div class="form-group">
            <asp:Label ID="lblCOnfirmarContrasena" AssociatedControlID="txtConfrimarContrasena" runat="server" Text="Confirmar Contraseña"></asp:Label>
            <asp:TextBox ID="txtConfrimarContrasena" required="required" CssClass="form-control" TextMode="Password" runat="server"></asp:TextBox>
       </div>
        
        <div class="form-group">
            <asp:Button ID="btnRegistrar" CssClass="btn btn-primary" runat="server" Text="Registrarse" OnClick="btnRegistrar_Click" />
            <asp:HyperLink ID="hlRegresar" CssClass="btn btn-secondary" NavigateUrl="~/Default.aspx" runat="server">volver</asp:HyperLink>
        </div>     
    </div>
    </form>
</body>
</html>
