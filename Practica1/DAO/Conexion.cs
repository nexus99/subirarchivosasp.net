﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Practica1.DAO
{
    public class Conexion
    {
        protected string cadenaConexion;

        protected SqlConnection cnn;

        public Conexion()
        {
            cadenaConexion = @"Data Source =.;Initial Catalog= practica4 ;Trusted_Connection=True;";
            cnn = new SqlConnection(cadenaConexion);
        }

        public DataTable Consulta(SqlCommand cmd)
        {
            try
            {
                cnn.Open();
                cmd.Connection = cnn;
                cmd.ExecuteNonQuery();
                DataTable tabla = new DataTable();
                tabla.Load(cmd.ExecuteReader());
                cnn.Close();
                return tabla;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int Ejecutar(SqlCommand cmd)
        {
            try
            {
                cmd.Connection = cnn;
                cnn.Open();
                int numeroDeFilasAfectadas = cmd.ExecuteNonQuery();
                cnn.Close();
                return numeroDeFilasAfectadas;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}