﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Practica1.DAO;
using Practica1.Utelerias;
using System.Data;

namespace Practica1.Models
{
    public class Usuario : Conexion
    {
        public int id_usuario { get; set; }
        public string nombre { get; set; }
        public string contrasena { get; set; }
        public string apellidos { get; set; }
        public string fechaNacimiento { get; set; }
        public string correo { get; set; }
        string query;
        public bool validarCorreo()
        {
            try
            {
                query = "SELECT * FROM usuarios WHERE correo = @correo";
                SqlCommand cmd = new SqlCommand(query);
                cmd.Parameters.AddWithValue("@correo", correo);
                return (Consulta(cmd).Rows.Count > 0) ? true : false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        

        public int guardar()
        {
            try
            {
                query = "INSERT INTO usuarios(nombre,apellidos,correo,contrasena,Nacimiento) VALUES (@nombre,@apellidos,@correo,@contrasena,@fechaNacimiento)";
                SqlCommand cmd = new SqlCommand(query);
                cmd.Parameters.AddWithValue("@nombre", nombre);
                cmd.Parameters.AddWithValue("@apellidos", apellidos);
                cmd.Parameters.AddWithValue("@correo", correo);
                cmd.Parameters.AddWithValue("@fechaNacimiento", fechaNacimiento);
                cmd.Parameters.AddWithValue("@Contrasena", Encriptar.GetSHA1(contrasena));
                return Ejecutar(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool validarAcceso()
        {
            try
            {
                query = "SELECT * FROM usuarios WHERE usuario = @usuario AND contrasena = @contrasena";
                SqlCommand cmd = new SqlCommand(query);
                cmd.Parameters.AddWithValue("@usuario", nombre);
                cmd.Parameters.AddWithValue("@contrasena", Encriptar.GetSHA1(contrasena));
                DataTable usu = Consulta(cmd);
                // primero se recupera la fila y posterior la el valor a recuperar
                if (usu.Rows.Count > 0)
                {
                    id_usuario = (int)(usu.Rows[0]["id_usuario"]);
                }
                return (usu.Rows.Count > 0) ? true : false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}