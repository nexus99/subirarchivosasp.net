﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Practica1._default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Inicio</title>
    <link href="Utelerias/bootstrap.min.css" rel="stylesheet" />
    <style>
        body {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            display:flex;
            justify-content:center;
            align-items:center;
        }

        .carta-fix {
            width:25%;
            padding:24px;
        }
     </style>
</head>
<body>
    <form id="form1" class="carta-fix" runat="server">
    <div >
        <div class="form-group">
            <asp:Label ID="lblCorreo"  AssociatedControlID="txtCorreo" runat="server" Text="Correo"></asp:Label>
            <asp:TextBox ID="txtCorreo" CssClass="form-control"  required="required" runat="server"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label ID="lblContrasena" AssociatedControlID="txtContrasena" runat="server" Text="Contraseña"></asp:Label>
            <asp:TextBox ID="txtContrasena" CssClass="form-control" required="required" TextMode="Password" runat="server"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Button ID="btnIniciar" CssClass="btn btn-primary" runat="server" Text="Iniciar Sesión"  />
        </div>
    </div>
    </form>
</body>
</html>
